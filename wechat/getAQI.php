<?php
// 获取空气质量AQI函数
function getAQI($fromUsername,$toUsername){
	$textTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[%s]]></MsgType>
                <Content><![CDATA[%s]]></Content>
                <FuncFlag>0</FuncFlag>
                </xml>";                          //构造XML数据格式
	$msgType = "text";                         //定义响应消息类型text	

	/*********业务逻辑开始*******/ 
	$url="http://apistore.baidu.com/microservice/aqi?city=深圳";
	$aqiJson = file_get_contents($url); 					//获取json信息
	$aqi = json_decode($aqiJson,true);  					//解析json为数组,默认为对象
	$contentStr = "数据采集时间: ".$aqi['retData']['time']."\n空气质量指数AQI: ".$aqi['retData']['aqi']."\n空气等级: ".$aqi['retData']['level']."\n首要污染物: ".$aqi['retData']['core'];
	/*********业务逻辑结束*******/

	$responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    	//把格式化的字符串写入变量
    	echo $responseStr;                             //响应XML数据
}
?>