<?php


  /*******************************
  技术支持： 唤梦科技
  website : http://www.dreammm.net
  QQ 75039960
  Mobile 18665802636
  ********************************/

/*** 创建自定义菜单 ***/

$json = '{
  "button":[


      {
          "name":"今日报价",
          "sub_button":[
            {
               "type":"click",
               "name":"简单介绍",
               "key":"introduce"
            },
            {
               "type":"click",
               "name":"在线客服",
               "key":"online_service"
            },
            {
               "type":"click",
               "name":"今日报价",
               "key":"today_quote"
            }]
        },

        {
           "name":"鉴别真假",
           "sub_button":[
            {
               "type":"view",
               "name":"6改6s鉴别",
               "url":"http://v.youku.com/v_show/id_XMTQwMzczNjM4OA==.html"
            },
            {
                "type":"view",
               "name":"6s原封鉴别",
               "url":"http://v.youku.com/v_show/id_XMTM3NzAyNzc5Ng==.html"
            },
             {
                "type":"view",
               "name":"i7原封鉴别",
               "url":"http://www.toutiao.com/i6334478608760832514/"
            },
              {
                "type":"view",
               "name":"i7高仿鉴别",
               "url":"http://www.toutiao.com/i6408349629993189890/"
            },
            {
               "type":"click",
               "name":"更多教程",
               "key":"more_course"
            }
            ]
       },
        {
               "type":"click",
               "name":"合作批发",
               "key":"wholesale"
        }

]}';

function create_menu()
{

    include_once   ('get_access_token.php');                 //引入获取access_token文件
    //include_once   ('json_menu.php');                          //引入菜单json配置文件

    $access_token = get_access_token();             //获取可用的access_token
    $jsonMenu = $json;                                  //json菜单
    $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$access_token;   //请求地址
    $result = https_request($url, $jsonMenu);      //执行请求函数//  自动执行吗 ？
    var_dump($result);
}

//请求函数
function https_request($url,$data = null){
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if (!empty($data)){
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}
?>