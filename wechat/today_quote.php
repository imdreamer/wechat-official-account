<?php

include_once ("common.php");

function today_quote($object)
{
    logWrite(  " im  in today_quote ..") ;

    $content = array();

    //1
    $content[] = array(
    "Title"=>"【苹果最新报价】苹果全系列渠道批发报价",
    "Description" => "关于我们梦想青年科技是一家专业从事电子产品销售的公司，梦想青年科技作为大学生创业公司在创建两年的时间里不断的",
    "PicUrl"=>"http://mmbiz.qpic.cn/mmbiz_jpg/ZGNUlSAMpwxQOofcickWsx7Ty2gLL7Xrg5QLe7m4g7XgOfVspribz6zZ72IG2qjoib0enVgPG2q5J0TZrGyZ89IUg/0?wx_fmt=jpeg",
    "Url" =>"http://mp.weixin.qq.com/s?__biz=MzA3NjM3MzUwNA==&mid=407826809&idx=1&sn=4aba210bd4ffdf7d54bf1163ed9623f0&chksm=02d81a1635af9300afc53b14b1f3c04a01c709ac680bace0249bceb0b2b7207485c467050a6e#rd"
    );


   //2
  $content[] = array(
    "Title"=>"安卓/window产品/各牌配件报价",
    "Description" => "以下为全系列安卓手机渠道报价零售规则为：0-1000元 溢价50元1000-2000元溢价80元2000元以",
    "PicUrl"=>"http://mmbiz.qpic.cn/mmbiz_png/ZGNUlSAMpwzfv2rFwBPpLMpXjUTH8RnpIiaL0b4wvewGXTUESTbdb7sHlXmCPXUHmlt3836nNmkBXqx1C0Z5mSg/0?wx_fmt=png",
    "Url" =>"http://mp.weixin.qq.com/s?__biz=MzA3NjM3MzUwNA==&mid=407826809&idx=2&sn=b9417c2ff56f9da548565ed69d7a54c6&chksm=02d81a1635af930092f5c8c3396680e9cb1193de30184b0b9f79ef1010bd3695c1fed9ca9889#rd"
    );

   //3
  $content[] = array(
    "Title"=>"苹果产品维修/改装报价",
    "Description" => "关于梦想青年科技采用的配件已经在报价右下角有解释,请各位仔细阅读",
    "PicUrl"=>"http://mmbiz.qpic.cn/mmbiz_png/ZGNUlSAMpwwLAiaPh7rCDia2u6aFibn85d8FlXWJrd0Ok6Lq8DCJfrn1xuZTr5TervL01xdjfxFmYdlcBoKILrYyA/0?wx_fmt=png",
    "Url" =>"http://mp.weixin.qq.com/s?__biz=MzA3NjM3MzUwNA==&mid=407826809&idx=3&sn=6891da1a3c81d88cf1a78bae20f7d41e&chksm=02d81a1635af93002542c7b6cedfba6ffaa69694bf35fa1413b3867d9a737ba32843f743b4d5#rd"
    );
   //4
  $content[] = array(
    "Title"=>"三星Galaxy S8/S8+微博修改小尾巴，引来网友众怒",
    "Description" => "每个人在玩微博的时候，想必都会对微博来源进行了解，而大多数的微博来源是使用某个客户端形成的，除了这些，还有来",
    "PicUrl"=>"http://mmbiz.qpic.cn/mmbiz_jpg/ZGNUlSAMpwwjrkzAeNw9PIthggibx000xmFPv6ZXY2boibJYqrlEohpML2pMLEjDfBQnqIWHnooKnRHFsoAibbdkg/0?wx_fmt",
    "Url" =>"http://mp.weixin.qq.com/s?__biz=MzA3NjM3MzUwNA==&mid=407826809&idx=4&sn=e0c0d6a197c2df52e4ae8409cb3733c8&chksm=02d81a1635af930055329662696301164dd6d68c37f3b67b97b1e9e664715c7281ad6a9110ac#rd"
    );


    /*
   //5
  $content[] = array(
    "Title"=>"关于公众号暂停自动回复通知",
    "Description" => "各位梦想青年科技的客户/用户你们好,由于近期梦想青年科技的微信公众号正在开发第三方功能,所以公众号目前所有自",
    "PicUrl"=>"http://mmbiz.qpic.cn/mmbiz_png/ZGNUlSAMpwwjrkzAeNw9PIthggibx000x9BxphQibQeK8FWrBO2DLHP9hEfFX9zcJ6oIF9UWnOeuorXafe6ic5EPw/0?wx_fmt=png",
    "Url" =>"http://mp.weixin.qq.com/s?__biz=MzA3NjM3MzUwNA==&mid=407826809&idx=5&sn=a74b9baba5dd79c9d985d55d188cb88a&chksm=02d81a1635af930086dfce0ffb3847677638564d73b0a7a9e9bf40547d6e77297090d11dec9b#rd"
    );
*/

    if(is_array($content))
    {
        if (isset($content[0]['PicUrl']))
        {
              $result = transmitNews($object, $content);
              echo  $result;
        }
    }

    return;
}

?>