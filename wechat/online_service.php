<?php
//关注事件函数
function online_service($fromUsername,$toUsername){
    $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";                          //构造XML数据格式
    $msgType = "text";                              //定义响应消息类型text

    /*********业务逻辑开始*******/ 
    $contentStr = "如在查看报价/对版本有不了解的情况下,请联系本店以下工作人员进行咨询:

微信：mxmxcn
QQ2442252620,
QQ:825471178 （已满人请勿添加）
淘宝：梦想青年科技
电话：18707157593";
    /*********业务逻辑结束*******/

    $responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    //把格式化的字符串写入变量
    echo $responseStr;                             //响应XML数据
}
?>