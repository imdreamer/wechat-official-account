<?php
// 未知文本或表情信息处理函数
function textUnknown($fromUsername,$toUsername){
    $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";                          //构造XML数据格式
    $msgType = "text";                              //定义响应消息类型text
    $contentStr = "你好,感谢您向梦想青年科技咨询问题。\n
但是公众号并非人工智能无法自动回复您的问题,\n
若有手机问题以及购机方面的疑问可以添加微信客服：sy825471178\n
或者添加QQ群301881287进行咨询均可。\n";
    $responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    //把格式化的字符串写入变量
    echo $responseStr;                             //响应XML数据
}



?>