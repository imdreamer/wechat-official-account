<?php
header("Content-Type: text/html;charset=utf-8");

 /*******************************
  技术支持： 唤梦科技
  website : http://www.dreammm.net
  QQ 75039960
  Mobile 18665802636
  ********************************/

/***** 浏览器访问处理 *****/
if (count($_GET)<=2)
{
    //http://www.dreammm.net/wchat/index.php   访问场景
	exit("<center><h1> 技术支持：<a  href='http://www.dreammm.net'>唤梦科技QQ 75039960,Mobile 18665802636 </a> </h1></center>");
}

/***** 微信服务器验证 *****/
require_once ("verify.php");					//引入微信服务器验证文件
$wechatObj = new verify();			//新建微信服务器验证类对象
if (isset($_GET['echostr'])){
    $wechatObj->valid();             		//执行微信服务器验证函数
}


include_once ("create_menu.php");
create_menu();


/*----------------------------------公众号功能----------------------------------------*/

//接受POST过来的XML数据,或file_get_contents('php://input')
$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
//为空则脚本终止
if (empty($postStr)){ 
	echo "";
	exit;
}
else{
	//解析XML数据为对象
	$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
	$from = $postObj->FromUserName;   		//获取XML消息发送方
	$to = $postObj->ToUserName;        		//获取XML消息接收方

	$msgType = trim($postObj->MsgType); 	//获取XML消息类型
	$event  =null;

	$title="unknown" ;

	/***响应文本或表情信息***/
	if($msgType == "text")
	{
		$keyword =trim($postObj->Content);
		//$len= mb_strlen($keyword,'UTF8');  //sn=12, imei =

		$imeiPattern='/^[0-9]{15}$/';
		$snPattern='/^[A-Za-z0-9]{11,12}$/';
        $imei=preg_match($imeiPattern, $keyword);
        $sn=preg_match($snPattern, $keyword);

        if($sn)
            if(preg_match("/^[a-z]*$/i",$keyword))
                 $sn = false;


        if($imei)
        {
  		    include_once  "db2.php";
     		     if(checkAccessCount($postObj,"imei",$keyword))
     		     {

     		         if(true)
     		         {

                        ignore_user_abort(true);
                         ob_start();

                        include_once  ("common.php");

  			           $content = "正在努力查询中,请稍等片刻";
  			           //transmitText($postObj, $content);
  			            send_mess($postObj,"正在努力查询中,请稍等片刻.");

  			            //sleep(8);


                        include_once  ("phoneImeiInfo.php");
				       if( phoneImeiInfo($postObj,$keyword,true))
				            addOperationRecord($postObj,"imei",$keyword);

                          include_once ("test.php");
                         test4($postObj, "此查询结果来自微信公众号：梦想青年科技,若查询有误请参考苹果官网www.apple.com.cn");//欢迎下次光临!

                         header('Connection: close');
                         header('Content-Length: ' . ob_get_length());
                       ob_end_flush();
                        ob_flush();
                         flush();

                         }



     		     }
     		     else
     		     {
     		            //addOperationRecord($postObj,"imei",$keyword);
                      include_once ("test.php");
                      test4($postObj, "你当日查询次数已经超过3次，请明日再来");
                        exit;
     		     }
     		     exit;

        }
        elseif ($sn)
        {


     		    include_once  "db2.php";
     		     if(checkAccessCount($postObj,"sn",$keyword))
     		     {
     		     /*
                        addOperationRecord($postObj,"sn",$keyword);
                        include_once  ("common.php");
  			           send_mess($postObj,"正在努力查询中,请稍等片刻");
  			           include_once  ("phoneImeiInfo.php");
				        phoneImeiInfo_2($from,$to,$keyword,false);
				        */

                       if(true)
                       {
				         ignore_user_abort(true);
                         ob_start();
                       }

                        include_once  ("common.php");

  			           $content = "正在努力查询中,请稍等片刻";
  			           //transmitText($postObj, $content);
  			            send_mess($postObj,"正在努力查询中,请稍等片刻");

                        include_once  ("phoneImeiInfo.php");
				       if( phoneImeiInfo($postObj,$keyword,0))
				            addOperationRecord($postObj,"sn",$keyword);


				        //sleep(8);

                         if(true)
                         {
                             include_once ("test.php");
                             test4($postObj, "此查询结果来自微信公众号：梦想青年科技,若查询有误请参考苹果官网www.apple.com.cn");//欢迎下次光临!
                         }


                     if(true)
                     {
                         header('Connection: close');
                         header('Content-Length: ' . ob_get_length());
                       ob_end_flush();
                        ob_flush();
                         flush();
                     }
     		     }
     		     else
     		     {
     		           //addOperationRecord($postObj,"sn",$keyword);
                      //include_once  ("common.php");

                      include_once ("test.php");
                      test4($postObj, "你当日查询次数已经超过3次，请明日再来");
                      //send_mess($postObj,"你当日查询次数已经超过3次，请明日再来");
     		     }
				exit;
        }

         if( strlen($keyword)>=5)
         {
             if(strcmp( substr($keyword,0,5),"news#" )==0)
             {
	              include_once "support.php";
	             $array = array();
	               $content = substr($keyword,5 );
                    $array = explode('#',$content,4);

	                news($postObj,$array);
		      }


         }


      $matchWords = array( '报价','价格','多少钱','怎么卖');
      //foreach ($matchWords as   $word)
      foreach ($matchWords  as    $key => $word)
      {
           if($keyword==$word   ||  stristr($keyword, $word))   //stristr 和 stripos有个问题就是 从0开始的 就有问题了 和 匹配到了最后
           {
                    include_once "today_quote.php";
		            today_quote($postObj);
				    exit;
                   // break;
           }
      }

     //$matchIntroduce  这个变量名字必须重新定义，不能复用$matchWords
      $matchIntroduce = array( '你好','您好', '介绍','有人吗','亲', '在吗' ,  'hello');
      foreach ($matchIntroduce as $Item)
      {
           if($keyword==$Item   ||  stripos($keyword, $Item))
           {
                   include_once  ("introduce.php");			//介绍
				    introduce3($from,$to);
				    exit;
           }
      }


      switch($keyword)
      {
		 case 'database':
		            include_once "database.php";
		            TestDb($postObj,'TestDb');
				    break;

			case 'token':
			   include_once   ('get_access_token.php');                 //引入获取access_token文件
              GetTokenTest($from,$to);
				break;

			case 'test':
				include_once ("test.php");
				//test2($from,$to);
				//test3($postObj);
				test4($postObj, $keyword);
				break;
/*
		    case 'subscribe':
		        include_once  "db2.php";
		        addOperationRecord($postObj,"subscribe");

		        exit;
		    case 'unsubs':
  		        include_once  "db2.php";
		        addOperationRecord($postObj,"unsubscribe","unsubs");
			    exit;
*/

	       case 'jianzhi':
				include_once ("support.php");
				//test2($from,$to);
				//test3($postObj);
				//test4($postObj, $keyword);
				jianzhi($postObj);
				break;

             case "material":

             	 include_once  ("getMaterial.php");
             	 $type ="news" ;
             	 $offset = 0;
             	 $count = 20;

		          get_article_list($postObj, $type, $offset, $count);

                break;

			default:
				//require_once "robot.php";				//图灵机器人
				//robot($from,$to,$keyword);
				//break;
				include_once ("textUnknown.php");		//未知关键词响应
				textUnknown($from,$to);
				break;
		}// switch($keyword)

	    exit;

	}//if($msgType == "text")


	/***获取事件类型***/
     if($msgType == event)
     {
           $event = $postObj->Event;

		    if ($event == "CLICK")
	        {
		       switch ($postObj->EventKey)
		       {
		          case "wholesale":
		          {
             	    include_once "wholesale.php";

		            if(0)
		               wholesale_2($from,$to);
		            else
		               wholesale($postObj);

                    exit;
                     break;
                  }

                  case "online_service":
             	     include_once "online_service.php";
		             online_service($from,$to);
		             exit;

                 case "introduce":
             	     include_once "introduce.php";
		              introduce($postObj);
		              exit;
	              case "today_quote":
             	       include_once "today_quote.php";
		               //today_quote($from,$to);
		               today_quote($postObj);
		               exit;
                       break;
                    case "more_course":
             	         include_once "more_course.php";
		                 more_course($from,$to);
                         exit;

                     default:
                          break;

		         }//switch ($postObj->EventKey)

		          exit;

	     }//if ($event == "CLICK")
	     	/***响应关注事件***/
	     elseif ($event == "subscribe")
	     {
     	        include_once  "db2.php";
		        addOperationRecord($postObj,"subscribe");
		        exit;
	      }

	      /***响应取消关注事件***/
	      elseif ($event == "unsubscribe")
	      {
		        include_once  "db2.php";
		        addOperationRecord($postObj,"unsubscribe");
			    exit;

	       }

           exit;
     }// if($msgType == event)

     if(true)
      switch($msgType)
      {
      	 case 'image':
		     $title  = "图片";
		     break;

		 case 'voice':
		     $title  = "语音";
		     break;
		 case 'music':
		     $title  = "音乐";
		     break;
		 case 'video':
		     $title  = "视频";
		     break;
		 case 'location':
		     $title  = "位置";
		     break;
		 case 'link':
		     $title  = "链接";
		     break;
	 	default:
		     $title  = "其它";
		     break;


	  }

	 if($msgType == "voice"  || $msgType == "music"  || $msgType == "image" || $msgType == "video"  || $msgType == "location"  || $msgType == "link" )
	{
			include_once ("common.php");		//未知
			otherUnknown($from,$to,$title);
			exit;
	}
	else
	{
		exit;
	}

	exit;
}
?>