<?php
// 获取时间函数
function getTime($fromUsername,$toUsername){
    $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";                          //构造XML数据格式
    $msgType = "text";                              //定义响应消息类型text

    /*********业务逻辑开始*******/ 
    $contentStr = date("Y-m-d H:i:s",time());   //获取当前时间
    /*********业务逻辑结束*******/

    $responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    //把格式化的字符串写入变量
    echo $responseStr;                             //响应XML数据
}
?>