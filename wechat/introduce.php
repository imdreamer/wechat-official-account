<?php


include_once ("common.php");

function introduce($object)
{
    logWrite(  " im  in introduce ..") ;

    $content = array();
    $content[] = array("Title"=>"梦想青年的自我介绍",
    "Description" => "一,梦想青年的简介：梦想青年这个名字虽然听起来很土,（至少我媳妇是这么认为的）但是这是我躺在大学宿舍的床上思",
    "PicUrl"=>"http://mmbiz.qpic.cn/mmbiz_png/ZGNUlSAMpwyNJu7GroSGmniaMT0icoawOQCz4YAhxuia7K9m9PsKjdgj7cppiapD5bC1cLptq2daM7MOmXbFZqAzBg/0?wx_fmt=png",
    "Url" =>"http://mp.weixin.qq.com/s?__biz=MzA3NjM3MzUwNA==&mid=509557842&idx=1&sn=c98b5d333d369eebccb1d2df3cca4249&chksm=04c9d73d33be5e2b8b859d1ba30f504129dbd7ebe54788bf3874d42b7825f8354969bab95a2f#rd"
    );

    if(is_array($content))
    {
        if (isset($content[0]['PicUrl']))
        {
              $result = transmitNews($object, $content);
              echo  $result;
        }
    }

    return;
}

function introduce2($fromUsername,$toUsername){
    $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";                          //构造XML数据格式
    $msgType = "text";                              //定义响应消息类型text



       $contentStr="一,梦想青年的简介：
梦想青年这个名字虽然听起来很土,（至少我媳妇是这么认为的）但是这是我躺在大学宿舍的床上思考了很多个夜晚才起的名字。看那些做大做强的企业都是两个简洁的名字,比如京东/天猫/百度/搜狗等等等等,但是既然我已经取了这个名字我就不想轻易的放弃,于是从2014年我们沿用至今。
创办之出,我是独自一人前往深圳这座汇聚了电子产品精英的城市,我的目的并不是想在这里开创多大的规模,成就多大的销量,而是能够了解清楚我从事的这行业渠道和前景究竟是什么样的。在不断的摸索和探索中我决定了自己前进的方向，决定了自己成长的方向。
价格透明,售后专业,保证品质,无限的内容传播是我们不变的宗旨。
在今后,我们会努力将梦想青年科技打造成为年轻人/大学生首选的电子产品购机平台。" ;



    $responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    //把格式化的字符串写入变量
    echo $responseStr;                             //响应XML数据
}



function introduce3($fromUsername,$toUsername){
    $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";                          //构造XML数据格式
    $msgType = "text";                              //定义响应消息类型text



      $contentStr = "您好,很高兴见到您，感谢您向梦想青年科技咨询问题。\n
但是公众号并非人工智能无法自动回复您的所有问题,\n
若有手机问题以及购机方面的疑问请您添加微信客服：sy825471178\n
或者添加QQ群301881287进行咨询，谢谢！\n";

    $responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    //把格式化的字符串写入变量
    echo $responseStr;                             //响应XML数据
}
?>