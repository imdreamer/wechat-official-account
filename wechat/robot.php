<?php
//图灵机器人
function robot($fromUsername,$toUsername,$keyword){
	$textTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[%s]]></MsgType>
                <Content><![CDATA[%s]]></Content>
                <FuncFlag>0</FuncFlag>
                </xml>";                          //构造XML数据格式
	$msgType = "text";                         //定义响应消息类型text	

	/*********业务逻辑开始*******/ 
	$url="http://www.tuling123.com/openapi/api?key=ee02e88432f702f4cba0c6588d3b0842&info=$keyword";
	$robotJson = file_get_contents($url); 					//获取json信息
	$robot = json_decode($robotJson,true);  				//解析json为数组,默认为对象,true参数为数组
	$contentStr = $robot['text'];
	/*********业务逻辑结束*******/

	$responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    	//把格式化的字符串写入变量
    	echo $responseStr;                             //响应XML数据
}
?>