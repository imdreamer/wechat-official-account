<?php
//获取微信服务器ip函数
function get_server_ip($token){
	$url="https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=$token";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	curl_close($ch);

	$jsoninfo = json_decode($output, true);		//json转数组
	return $jsoninfo['ip_list'];					//ip数组
}

require "access_token.php";
echo $a=get_access_token();
echo "<br>";
print_r(get_server_ip($a));
?>