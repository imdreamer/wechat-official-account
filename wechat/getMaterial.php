<?php
include_once ("common.php");

include_once   ("get_access_token.php");


function    my_https_request($url, $data = null)
{
       $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        if (!empty($data))
        {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curl);
        curl_close($curl);

         logWrite("my_https_request");
          logWrite($output);
        return $output;
  }



function get_article_list($postObj, $type, $offset, $count)
{
        $access_token = get_access_token();
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=".$access_token;
        $data = '{"type":"'.$type.'","offset":"'.$offset.'","count":"'.$count.'"}';

        //$response =  get_response_post($url, $data);
         $response =  my_https_request($url, $data);

        //echo strip_tags($response);
        //$res = json_decode($response, true);

     $responseStr=transmitText($postObj,$data);
      echo $responseStr;
}


function send_mess($postObj, $type, $offset, $count)
{
        $access_token = get_access_token();
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=".$access_token;
        $data = '{"type":"'.$type.'","offset":"'.$offset.'","count":"'.$count.'"}';

        //$response =  get_response_post($url, $data);
         $response =  my_https_request($url, $data);

        //echo strip_tags($response);
        //$res = json_decode($response, true);

     $responseStr=transmitText($postObj,$data);
      echo $responseStr;
}


?>