<?php
// helloWords API
function helloWords($fromUsername,$toUsername){
	$textTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[%s]]></MsgType>
                <Content><![CDATA[%s]]></Content>
                <FuncFlag>0</FuncFlag>
                </xml>";                          //构造XML数据格式
	$msgType = "text";                         //定义响应消息类型text	

	/*********业务逻辑开始*******/ 
	$url="http://hello.api.235dns.com/api.php?code=xml";
	$xml=file_get_contents($url); 			//获取xml信息
	$res=simplexml_load_string($xml);  		//解析xml成对象
	$contentStr="$res->words";
	/*********业务逻辑结束*******/

	$responseStr=sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    	//把格式化的字符串写入变量
    	echo $responseStr;                             //响应XML数据
}
?>