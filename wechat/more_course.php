<?php
//更多教程
function more_course($fromUsername,$toUsername){
    $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>";                          //构造XML数据格式
    $msgType = "text";                              //定义响应消息类型text

    /*********业务逻辑开始*******/ 
    $contentStr = "你们所看到的内容均是梦想青年科技的原创内容,也是梦想青年科技坚持做正品的源泉。不仅仅是保证手机是全新未激活,还向各位保证自己保证每个细节都不会作假.这是我们的立足之本,也是我们未来发展不变的精神。

如果各位需要观看其他方面的鉴别视频或者文章,可以通过以下方式了解：
今日头条签约作者： 梦想青年科技/阿强的梦想
优酷自媒体：梦想青年科技
土豆自媒体：梦想青年科技
百度百家签约自媒体：杨强
网易新闻自媒体：杨强
一点资讯自媒体：梦想青年

我们倾尽全力,只为让各位购买到放心的手机.";
    /*********业务逻辑结束*******/

    $responseStr = sprintf($textTpl, $fromUsername, $toUsername, time(), $msgType, $contentStr);
    //把格式化的字符串写入变量
    echo $responseStr;                             //响应XML数据
}
?>